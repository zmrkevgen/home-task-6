package oop;

public class Phones extends Devices {
    public int numberCards;

    Phones(int processorFrequency, int memory, double systemVersion, int vendorPrice, int numberCards) {
        super(processorFrequency, memory, systemVersion, vendorPrice);
        this.numberCards=numberCards;
    }
}
