package oop;

public class Iphone extends Phones implements FirstMessage {
    String model;
    int overprice = 150;
    MobilePhoneColors colors;
    public static final int EUROPE_COST_IPHONE = 1000;

    Iphone(int processorFrequency, int memory, double systemVersion, int vendorPrice, int numberCards, String model) {
        super(processorFrequency, memory, systemVersion, vendorPrice, numberCards);
        this.model=model;
    }


    @Override public int price(){
        return vendorPrice + traderInterest + overprice;
    }

    @Override
    public void message() {
        System.out.println("Hello, Iphone user");
    }
    @Override
    public void displayInfo(){
        System.out.println("Display size is 7'");

    }
    public  void setPhoneColors (MobilePhoneColors colors){
        this.colors=colors;
    }
    public MobilePhoneColors getColorsMobile()  {
        return colors;
    }
    public String getModel(){return model;}
    public void setModel(String model){this.model=model;}
    public static enum MobilePhoneColors{
        RED("red"), BLUE("blue"), WHITE("white");
        private final String colors;

        MobilePhoneColors(String colors) {
            this.colors=colors;
        }
    }

}
