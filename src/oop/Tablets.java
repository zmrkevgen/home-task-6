package oop;

public class Tablets extends Devices {
    public String vendor;

    Tablets(int processorFrequency, int memory, double systemVersion, int vendorPrice, String vendor) {
        super(processorFrequency, memory, systemVersion, vendorPrice);
        this.vendor=vendor;
    }
}
