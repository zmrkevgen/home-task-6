package oop;

public class Devices extends DisplaySize {
    public int processorFrequency;
    public int memory;
    public double systemVersion;
    public int vendorPrice;
    public int traderInterest = 100;

    Devices(int processorFrequency, int memory, double systemVersion, int vendorPrice){
        this.processorFrequency=processorFrequency;
        this.memory=memory;
        this.systemVersion=systemVersion;
        this.vendorPrice=vendorPrice;

    }
    public int price(){return this.vendorPrice + this.traderInterest;}


    @Override
    public void displayInfo() {

    }
}
