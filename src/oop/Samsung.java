package oop;

public class Samsung extends Phones implements FirstMessage {
    public int countryTaxes;
    public static final int EUROPE_COST_SAMSUNG = 1000;
    Samsung(int processorFrequency, int memory, double systemVersion, int vendorPrice, int numberCards) {
        super(processorFrequency, memory, systemVersion, vendorPrice, numberCards);
    }
    @Override public int price(){
        return this.vendorPrice + this.traderInterest+this.countryTaxes;
    }

    @Override
    public void message() {
        System.out.println("Hello, Samsung user");
    }
    @Override
    public void displayInfo(){
        System.out.println("Display size is 8'");
    }}
